# darknet-gpu-docker #
Docker image that build and run darknet to train YOLOv4 on GPU  
For CPU-only image: https://gitlab.com/nkming2/darknet-cpu-docker

## Prerequisite ##
- Docker >= 19.03
- Docker Compose with PR [#7124](https://github.com/docker/compose/pull/7124) merged
	- https://github.com/beehiveai/compose
- NVIDIA Container Toolkit
	- https://github.com/NVIDIA/nvidia-docker

## Run ##
```cd build && docker-compose up -d && docker attach [NAME]```

## Train ##
- Place your files in the dirs under ```runtime/darknet```. These dirs will be bind mounted
- YOLOv4
	- ```./darknet detector train data/my.data cfg/my.cfg yolov4.conv.137 -dont_show```
- YOLOv4 tiny
	- ```./darknet detector train data/my.data cfg/my.cfg yolov4-tiny.conv.29 -dont_show```

## You may want to... ##
- Change TZ in Dockerfile. It defaults to UTC+0
- Update darknet
	- Replace ```build/darknet-master.zip``` with a new one from https://github.com/AlexeyAB/darknet
- Change the uid of the container user
	- Edit the line ```RUN useradd -u 1000 --create-home user``` in Dockerfile
